package bearman;

import food.Food;
import food.Pizza;
import food.Taco;

public class Bearman {
	
	/**
	 * Creates a bearman, and initializes him to hungry
	 */
	public Bearman() {
		hungry = true;
	}
	
	private boolean hungry;

	public boolean isHungry() {
		return hungry;
	}

	public void setHungry(boolean hungry) {
		this.hungry = hungry;
	}
	
	public void eat(Food f) {
		f.feedBearman(this);
	}
	
}
