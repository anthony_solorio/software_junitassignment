package bearman;

import food.Food;
import food.Pizza;
import food.Taco;

public class PizzaEatingBearman extends Bearman{
	public void eat(Food f) {
		f.feedBearman(this);
	}
}
